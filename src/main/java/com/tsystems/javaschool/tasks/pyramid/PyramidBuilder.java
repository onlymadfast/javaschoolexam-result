package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int size = inputNumbers.size();
        if (size > 50) throw new CannotBuildPyramidException();
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        //check,build pyramid or not
        int rows = 1;
        int cols = 1;
        int numb = 0;
        while (numb < size) {
            numb = numb + rows;
            rows++;
            cols = cols + 2;
        }
        //number of rows and columns that will be in the pyramid
        rows = rows - 1;
        cols = cols - 2;
        if (numb != size) throw new CannotBuildPyramidException();

        Collections.sort(inputNumbers);

        //fill pyramid
        int[][] pyramid = new int[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                pyramid[i][j] = 0;
            }
        }
        //build pyramid
        int center = (cols / 2);
        int count = 1;
        int offset = 0;
        int listIdx = 0;
        for (int i = 0; i < rows; i++) {
            int start = center - offset;
            for (int j = 0; j < count * 2; j += 2) {
                pyramid[i][start + j] = inputNumbers.get(listIdx);
                listIdx++;
            }
            offset++;
            count++;
        }
        return pyramid;
    }


}
