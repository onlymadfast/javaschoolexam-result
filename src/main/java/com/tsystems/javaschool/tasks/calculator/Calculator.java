package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.equals("")) {
            return null;
        }

        if (statement.chars().filter(ch -> ch == ')').count() != statement.chars().filter(ch -> ch == '(').count())
            return null;

        try {
            double result = (double) (RPNToAnswer(ExprToRPN(statement)));

            if (result % 1 == 0) {
                return Integer.valueOf((int) result).toString();
            } else {
                return result + "";
            }

        } catch (Exception e) {
            return null;
        }
    }

    public double RPNToAnswer(String rpn) {
        String operand = new String();
        Stack<Double> stack = new Stack<>();

        for (int i = 0; i < rpn.length(); i++) {
            if (rpn.charAt(i) == ' ') continue;
            if (getPrior(rpn.charAt(i)) == 0) {
                while (rpn.charAt(i) != ' ' && getPrior(rpn.charAt(i)) == 0) {
                    operand += rpn.charAt(i++);
                    if (i == rpn.length()) break;
                }
                stack.push(Double.parseDouble(operand));
                operand = new String();
            }
            if (getPrior(rpn.charAt(i)) > 1) {
                double a = stack.pop(), b = stack.pop();

                if (rpn.charAt(i) == '+') stack.push(b + a);
                if (rpn.charAt(i) == '-') stack.push(b - a);
                if (rpn.charAt(i) == '*') stack.push(b * a);
                if (rpn.charAt(i) == '/') {
                    if (a == 0) throw new RuntimeException();
                    stack.push(b / a);

                }
            }
        }

        return stack.pop();
    }

    public String ExprToRPN(String Expr) {
        String current = "";
        Stack<Character> stack = new Stack<>();
        int priority;
        for (int i = 0; i < Expr.length(); i++) {
            priority = getPrior(Expr.charAt(i));
            if (priority == 0) current += Expr.charAt(i);
            if (priority == 1) stack.push(Expr.charAt(i));

            if (priority > 1) {
                current += ' ';
                while (!stack.empty()) {
                    if (getPrior(stack.peek()) >= priority) current += stack.pop();
                    else break;
                }
                stack.push(Expr.charAt(i));
            }
            if (priority == -1) {
                current += ' ';
                while (getPrior(stack.peek()) != 1) current += stack.pop();
                stack.pop();
            }
        }
        while (!stack.empty()) current += stack.pop();

        return current;
    }

    private int getPrior(char token) {
        if (token == '*' || token == '/') return 3;
        else if (token == '+' || token == '-') return 2;
        else if (token == '(') return 1;
        else if (token == ')') return -1;
        else return 0;
    }
}

